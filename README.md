<p align="center"><img src="https://res.cloudinary.com/dtfbvvkyp/image/upload/v1566331377/laravel-logolockup-cmyk-red.svg" width="400"></p>

## Content
1. Configuration .env file
2. Install laravelcollective/html and Run Migration
3. Configure js file

## Configuration .env file
- In this step, link your DB_DATABASE in .env file.

## Install laravelcollective/html and Run Migration
- Run > php artisan migrate
- composer require laravelcollective/html
- Next, add your new provider to the providers array and add two class aliases to the aliases array of config/app.php

## Configure js file
- You can go to https://www.mapbox.com/ and signup or signin and get your api key credential's.
- link you api key in mapInput.js and mapDisplay.js file > mapboxgl.accessToken = "XXXXX"

## About Mapbox
Mapbox is the location data platform for mobile and web applications. 

