$(function () {
    $(document).ready(function () {
        mapboxgl.accessToken = 'XXXX';

        if (window.location.pathname == '/') {
            var map = new mapboxgl.Map({
                container: 'map',
                style: 'mapbox://styles/mapbox/streets-v11',
                center: [73.8142, 15.5937],
                zoom: 13
            });

            var geocoder = new MapboxGeocoder({
                accessToken: mapboxgl.accessToken,
                countries: 'in',
                mapboxgl: mapboxgl
            });

            document.getElementById('geocoder').appendChild(geocoder.onAdd(map));

            geocoder.on('result', function (ev) {
                var lon = ev.result.geometry.coordinates[0];
                var lat = ev.result.geometry.coordinates[1];
                var place = ev.result.text;

                $('#longitude').val(lat);
                $('#latitude').val(lon);
                $('#place').val(place);
            });
        }
    });
});