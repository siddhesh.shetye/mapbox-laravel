$(function () {
    $(document).ready(function () {
        mapboxgl.accessToken = 'XXXX';

        if (window.location.pathname == '/viewlocation') {
            var longitude_address = JSON.parse($('#longitude_address').val());

            var map = new mapboxgl.Map({
                container: 'map',
                style: 'mapbox://styles/mapbox/light-v10',
                center: [73.8142, 15.5937],
                zoom: 9,
            });

            var geojson = {
                type: 'FeatureCollection',
                features: []
            };

            longitude_address.forEach(function (data) {
                let obj = {
                    type: 'Feature',
                    geometry: {
                        type: 'Point',
                        coordinates: []
                    },
                    properties: {
                        title: 'Address',
                        description: []
                    }
                };

                obj.geometry.coordinates = [data.latitude, data.longitude];
                obj.properties.description = [data.place];

                geojson.features.push(obj);
            });

            // add markers to map
            geojson.features.forEach(function (marker) {

                // create a HTML element for each feature
                var el = document.createElement('div');
                el.className = 'marker';

                // make a marker to feature and add to the map
                new mapboxgl.Marker(el)
                    .setLngLat(marker.geometry.coordinates)
                    .setPopup(new mapboxgl.Popup({ offset: 25 }) // add popups
                        .setHTML('<p style="margin-bottom: 0;">' + marker.properties.description + '</p>'))
                    .addTo(map);
            });
        }
    });
});