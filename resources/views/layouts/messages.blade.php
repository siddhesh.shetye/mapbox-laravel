@if(count($errors) > 0)
@foreach($errors->all() as $error)
<div class="alert alert-danger" role="alert" style="width: auto; margin: 0 23%;">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
            aria-hidden="true">&times;</span></button>
    <strong>{{$error}}</strong>
</div>
@endforeach
@endif

@if(session('success'))
<div class="alert alert-success" role="alert" style="width: auto; margin: 0 23%;">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
            aria-hidden="true">&times;</span></button>
    <strong>Success!</strong> {{session('success')}}
</div>
@endif

@if(session('error'))
<div class="alert alert-danger" role="alert" style="width: auto; margin: 0 23%;">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
            aria-hidden="true">&times;</span></button>
    <strong>Alert!</strong> {{session('error')}}
</div>
@endif