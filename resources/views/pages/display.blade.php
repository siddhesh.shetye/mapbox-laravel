@extends('inc.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <input type="hidden" name="longitude_address" id="longitude_address" value="{{$location}}" />

            <div style="height: 450px;">
                <div id="map" class="map" style="height: inherit; width: inherit; position: relative;">
                </div>
            </div>
        </div>
    </div>
</div>

@endsection