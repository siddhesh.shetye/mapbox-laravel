@extends('inc.app')

@section('content')
<div class="container pd-30">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"> MapBox </div>
                <div class="card-body">

                    {!! Form::open(['action' => ['LocationController@store'] , 'method' => 'POST',
                    'class' => 'locationform', 'enctype' => 'multipart/form-data']) !!}
                    {{ csrf_field() }}

                    <div class="row">
                        <div class="col-md-12">
                            <h4 class="pad-10 bg-aqua">Location details</h4>
                        </div>
                    </div>
                    <hr>

                    <div class="row justify-content-center">
                        <div class="form-group col-md-10">
                            <div id="map"></div>
                            <div style="height:400px; ">
                                <div class="geocoder" id="geocoder"></div>
                            </div>

                            <input type="hidden" name="longitude" id="longitude" value="0" />
                            <input type="hidden" name="latitude" id="latitude" value="0" />
                            <input type="hidden" name="place" id="place" value="" />
                        </div>
                    </div>
                    <hr>

                    <button type="submit" class="btn btn-primary mr-2">Submit</button>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection